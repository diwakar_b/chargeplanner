package ee.energia.testassignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

import ee.energia.testassignment.planning.ChargePlan;
import ee.energia.testassignment.price.EnergyPrice;

@Component
public class ChargePlanner {

	// TODO Add slf4j logger

	// the capability of the charger to charge certain amount of energy into the
	// battery in 1 hour
	public final static int CHARGER_POWER = 50;

	// maximum battery level possible
	public final static int MAX_LEVEL = 100;

	// battery level required by the end of the charging
	public final static int REQUIRED_LEVEL = 100;

	/**
	 * Method calculates the optimal hourly charge plan. Method finds the cheapest
	 * hour to charge the battery (if multiple then the earliest) and uses it to
	 * charge the battery up to the {@link ChargePlanner#REQUIRED_LEVEL}. If
	 * {@link ChargePlanner#CHARGER_POWER} limitation does not allow to do this in
	 * one hour, then method finds the next cheapest opportunities and uses them
	 * until {@link ChargePlanner#REQUIRED_LEVEL} is met.
	 *
	 * Method returns the array of {@link ChargePlan} objects that represent the
	 * hourly time slot and the capacity that we need to charge during that hour to
	 * charge the battery.
	 *
	 * @param batteryLevel initial battery level when the charger is connected
	 * @param energyPrices the list of the energy prices from the moment when
	 *                     charger is connected until the moment when battery needs
	 *                     to be charged there is an assumption that battery is
	 *                     connected the first second of the first given hour and
	 *                     disconnected the last second of the last given hour
	 * @return
	 */
	public static List<ChargePlan> calculateChargePlan(int batteryLevel, List<EnergyPrice> energyPrices,
			boolean calcChargeByOptimalCost) {
		List<ChargePlan> chargePlan = new ArrayList<>();
		if (energyPrices != null && energyPrices.size() > 0) {
		//	System.out.println("Initializing charge with batteryLevel: " + batteryLevel);

			// Step 1: Optimal Charge considering the Cost and hour
			if (calcChargeByOptimalCost) {
				// Sort by Cost
				sortEnergyPrice(energyPrices);
				//logger.debug	System.out.println("Optimized for Least Cost:" + energyPrices);
			} else {
				// No sorting in classical approach
				//logger.debug	System.out.println("Classical Cost:" + energyPrices);
			}

			// Step 2: Calculate optimal Plan by sorted List
			chargePlan = calcOptimalChargePlan(batteryLevel, energyPrices);
			//logger.debug	System.out.println("ChangePlan: " + chargePlan);
		}
		return chargePlan;
	}

	/**
	 * Calc optimal Plan logic
	 * 
	 * @param batteryLevel
	 * @param energyPrices
	 * @return
	 */
	private static List<ChargePlan> calcOptimalChargePlan(int batteryLevel, List<EnergyPrice> energyPrices) {
		int capacity = 0;
		List<ChargePlan> chargePlan = new ArrayList<>();
		for (EnergyPrice energyPrice : energyPrices) {
			if (batteryLevel < REQUIRED_LEVEL) {
				int tobeCharged = ChargePlanner.REQUIRED_LEVEL - batteryLevel;
				capacity = Math.min(tobeCharged, ChargePlanner.CHARGER_POWER);
				batteryLevel += capacity;
				chargePlan.add(new ChargePlan(capacity, energyPrice.getHour(), energyPrice.getMonth(),
						energyPrice.getYear(), energyPrice.getAskPrice()));
			} else {
				chargePlan.add(new ChargePlan(0, energyPrice.getHour(), energyPrice.getMonth(), energyPrice.getYear(),
						energyPrice.getAskPrice()));
			}
		}
		return chargePlan;
	}

	// Sort EnergyPrice by asking price and hour
	private static void sortEnergyPrice(List<EnergyPrice> energyPrices) {
		Collections.sort(energyPrices,
				Comparator.comparing(EnergyPrice::getAskPrice).thenComparingInt(EnergyPrice::getHour));
	}

	/**
	 * Calculate the cost difference (Classical cost Calc - Optimal Cost Calc)
	 * 
	 * @param optimalChargePlanCalc
	 * @param classicalChargePlanCalc
	 * @return
	 */
	public int calculateCostDifference(List<ChargePlan> optimalChargePlanCalc,
			List<ChargePlan> classicalChargePlanCalc) {
		int optiCost = 0;
		int classicCost = 0;
		for (ChargePlan optiChargePlan : optimalChargePlanCalc) {

			if (optiChargePlan.getCapacity() > 0) {
				optiCost += optiChargePlan.getCost();
			}
		}

		for (ChargePlan classicChargePlan : classicalChargePlanCalc) {

			if (classicChargePlan.getCapacity() > 0) {
				classicCost += classicChargePlan.getCost();
			}
		}
		return classicCost - optiCost;
	}
}