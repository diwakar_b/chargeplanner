package ee.energia.testassignment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ee.energia.testassignment.planning.ChargePlan;
import ee.energia.testassignment.price.EnergyPrice;

@SpringBootApplication
public class TestAssignmentApplication implements CommandLineRunner {

	//TODO Slf4j
	
	@Autowired
	private ChargePlanner chargePlanner;

	public static void main(String[] args) {
		SpringApplication.run(TestAssignmentApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		List<EnergyPrice> energyPrices = getEnergyPrices();
		System.out.println("Energy Prices: " + energyPrices);
		
		// By Optimal Cost calculation
		List<ChargePlan> optimalChargePlanCalc = chargePlanner.calculateChargePlan(20, energyPrices, true);
		System.out.println("Optimal: " + optimalChargePlanCalc);

		energyPrices = getEnergyPrices();
		// By Classical Cost calculation
		List<ChargePlan> classicalChargePlanCalc = chargePlanner.calculateChargePlan(20, energyPrices, false);
		System.out.println("Classical: " + classicalChargePlanCalc);

		// Get savings by using the Optimal Cost calculation over Classical Cost
		// calculation
		int savings = chargePlanner.calculateCostDifference(optimalChargePlanCalc, classicalChargePlanCalc);
		System.out.println("OptiChargePlan saved: [" + savings + "] dollors over the Classical approach.");
	
		System.exit(0);
	}

	// As per data from markdown
	private List<EnergyPrice> getEnergyPrices() {
		List<EnergyPrice> epList = new ArrayList<>();
		epList.add(new EnergyPrice(13, 10, 1, 07, 8, 2019));
		epList.add(new EnergyPrice(10, 9, 2, 07, 8, 2019));
		epList.add(new EnergyPrice(8, 7, 3, 07, 8, 2019));
		epList.add(new EnergyPrice(10, 9, 4, 07, 8, 2019));
		epList.add(new EnergyPrice(8, 7, 5, 07, 8, 2019));
		epList.add(new EnergyPrice(10, 8, 6, 07, 8, 2019));
		epList.add(new EnergyPrice(11, 9, 7, 07, 8, 2019));
		epList.add(new EnergyPrice(15, 13, 8, 07, 8, 2019));
		return epList;
	}
}
