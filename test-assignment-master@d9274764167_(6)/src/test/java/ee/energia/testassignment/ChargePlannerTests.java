package ee.energia.testassignment;

import ee.energia.testassignment.planning.ChargePlan;
import ee.energia.testassignment.price.EnergyPrice;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
public class ChargePlannerTests {

	@Test
	public void chargePlannerReturnsDefinedPlanForDefinedPrices() {
		int batteryLevel = 20;
		final ArrayList<EnergyPrice> energyPrices = TestDataUtility.getDefinedEnergyPriceSequence();
		final ArrayList<ChargePlan> definedPlan = TestDataUtility.getExpectedDefinedChargePlan(batteryLevel);

		// Sorting the output
		Collections.sort(definedPlan,
				Comparator.comparing(ChargePlan::getCapacity).thenComparingInt(ChargePlan::getHour));

		final List<ChargePlan> chargePlan = ChargePlanner.calculateChargePlan(batteryLevel, energyPrices, true);

		// Sorting the output
		Collections.sort(chargePlan,
				Comparator.comparing(ChargePlan::getCapacity).thenComparingInt(ChargePlan::getHour));

		Assert.assertEquals(definedPlan, chargePlan);
	}

	/**
	 * Test with null Energy price
	 * 
	 * @param batteryLevel
	 * @return
	 */
	@Test
	public void chargePlannerTest_SuccessWithNullEnergyList() {
		final List<ChargePlan> chargePlan = ChargePlanner.calculateChargePlan(30, null, true);
		Assert.assertTrue(chargePlan.isEmpty());
	}

	/**
	 * Test with Constant Energy Price
	 * 
	 * @return
	 */
	@Test
	public void chargePlannerTest_SuccessWithConstantEnergyList() {
		int batteryLevel = 20;
		final ArrayList<EnergyPrice> energyPrices = TestDataUtility.getDefinedEnergyPriceWithConstantCost();
		final ArrayList<ChargePlan> definedPlan = TestDataUtility
				.getExpectedDefinedChargePlanForConstantEnergyPrice(batteryLevel);
		// Sorting the output
		Collections.sort(definedPlan,
				Comparator.comparing(ChargePlan::getCapacity).thenComparingInt(ChargePlan::getHour));
		final List<ChargePlan> chargePlan = ChargePlanner.calculateChargePlan(batteryLevel, energyPrices, true);
		// Sorting the output
		Collections.sort(chargePlan,
				Comparator.comparing(ChargePlan::getCapacity).thenComparingInt(ChargePlan::getHour));
		Assert.assertEquals(definedPlan, chargePlan);
	}

	/**
	 * Test with random Preloaded Battery change
	 * 
	 * 
	 * @return
	 */
	@Test
	public void chargePlannerTest_SuccessWithRandomPreLoadedBatteryChange() {
		int batteryLevel = 65;
		final ArrayList<EnergyPrice> energyPrices = TestDataUtility.getDefinedEnergyPriceSequence();
		final ArrayList<ChargePlan> definedPlan = TestDataUtility.getExpectedDefinedChargePlan(batteryLevel);
		// Sorting the output
		Collections.sort(definedPlan,
				Comparator.comparing(ChargePlan::getCapacity).thenComparingInt(ChargePlan::getHour));
		final List<ChargePlan> chargePlan = ChargePlanner.calculateChargePlan(batteryLevel, energyPrices, true);
		// Sorting the output
		Collections.sort(chargePlan,
				Comparator.comparing(ChargePlan::getCapacity).thenComparingInt(ChargePlan::getHour));
		Assert.assertEquals(definedPlan, chargePlan);
	}
}
