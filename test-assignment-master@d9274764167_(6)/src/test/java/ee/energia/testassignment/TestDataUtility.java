package ee.energia.testassignment;

import java.util.ArrayList;

import org.junit.Assert;

import ee.energia.testassignment.planning.ChargePlan;
import ee.energia.testassignment.price.EnergyPrice;

/**
 * This is just data prep utility for testCases.
 * 
 * 
 * @author dbdavidraj
 *
 */
public class TestDataUtility {

	public static ArrayList<EnergyPrice> getDefinedEnergyPriceWithConstantCost() {
		final ArrayList<EnergyPrice> energyPrices = new ArrayList<>();
		energyPrices.add(new EnergyPrice(5, 4, 1, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 2, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 3, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 4, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 5, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 6, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 7, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(5, 4, 8, 1, 1, 2019));
		return energyPrices;
	}

	public static ArrayList<EnergyPrice> getDefinedEnergyPriceSequence() {
		final ArrayList<EnergyPrice> energyPrices = new ArrayList<>();
		energyPrices.add(new EnergyPrice(13, 10, 1, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(10, 9, 2, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(8, 7, 3, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(10, 9, 4, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(8, 7, 5, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(10, 8, 6, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(11, 9, 7, 1, 1, 2019));
		energyPrices.add(new EnergyPrice(15, 13, 8, 1, 1, 2019));
		return energyPrices;
	}

	public static ArrayList<ChargePlan> getExpectedDefinedChargePlan(int batteryLevel) {
		final ArrayList<ChargePlan> definedPlan = new ArrayList<>();
		definedPlan.add(new ChargePlan(0, 1, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 2, 1, 2019, 0));

		int capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
		definedPlan.add(new ChargePlan(capacity, 3, 1, 2019, 0));
		batteryLevel = capacity + batteryLevel;

		definedPlan.add(new ChargePlan(0, 4, 1, 2019, 0));

		capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
		definedPlan.add(new ChargePlan(capacity, 5, 1, 2019, 0));
		batteryLevel = capacity + batteryLevel;

		definedPlan.add(new ChargePlan(0, 6, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 7, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 8, 1, 2019, 0));

		Assert.assertEquals(batteryLevel, ChargePlanner.REQUIRED_LEVEL);

		return definedPlan;
	}

	public static ArrayList<ChargePlan> getExpectedDefinedChargePlanForConstantEnergyPrice(int batteryLevel) {
		final ArrayList<ChargePlan> definedPlan = new ArrayList<>();

		int capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
		definedPlan.add(new ChargePlan(capacity, 1, 1, 2019, 0));
		batteryLevel = capacity + batteryLevel;

		capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
		definedPlan.add(new ChargePlan(capacity, 2, 1, 2019, 0));
		batteryLevel = capacity + batteryLevel;

		definedPlan.add(new ChargePlan(0, 3, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 4, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 5, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 6, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 7, 1, 2019, 0));
		definedPlan.add(new ChargePlan(0, 8, 1, 2019, 0));

		Assert.assertEquals(batteryLevel, ChargePlanner.REQUIRED_LEVEL);

		return definedPlan;
	}
}
